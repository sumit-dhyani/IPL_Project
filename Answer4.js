import { matchData,deliveryData } from "./Project.js";


var matchIds=[]
matchData.forEach(match=>{
    if(match.season==2015){
        matchIds.push(match.id)
    }
})

var economyBowler={}
var overs={}
deliveryData.forEach(delivery=>{
    if(matchIds.includes(delivery.match_id)){
        if(economyBowler[delivery.bowler]){
            economyBowler[delivery.bowler]+=parseInt(delivery.total_runs)
            overs[delivery.bowler]+=1;
        }
        else
            {economyBowler[delivery.bowler]=parseInt(delivery.total_runs);
            overs[delivery.bowler]=1;}
    }
})

for(let i in overs){
    overs[i]/=6
}
console.log(overs)

for (let key in economyBowler){
    economyBowler[key] = Math.round((economyBowler[key]/overs[key]) * 100)/100;
  }

var economy = Object.keys(economyBowler).map(function(key) {
    return [key, economyBowler[key]];
  }).sort(function(a, b){ return a[1] - b[1]; });

console.log("For the year 2015 top 10 economical bowlers\n",economy.filter((item,i)=>i<10).map(item=>item[0]))
  
  