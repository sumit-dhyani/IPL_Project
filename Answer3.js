import {deliveryData,matchData}from "./Project.js";
const matchIds=[]
matchData.forEach(match=>{
    if(match.season==2016){
        matchIds.push(match.id)
    }
})
// console.log(matchIds)
let extraRuns={}

    deliveryData.forEach(delivery=>{
        if(matchIds.includes(delivery.match_id)){
            if(extraRuns[delivery.bowling_team]){
                extraRuns[delivery.bowling_team]+=parseInt(delivery.extra_runs);

            }
            else{
                extraRuns[delivery.bowling_team]=parseInt(delivery.extra_runs);
            }
        }
    })

console.log("For the year 2016 extra runs conceded per team.\n",extraRuns)